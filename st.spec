Name:		simple-terminal
Version:	0.8.2
Release:        3
Summary:	Suckless terminal (st) developed by suckless.org

License:	GPLv3+
URL:		https://gitlab.com/jeremieglbd/simple-terminal/
Source0:	https://gitlab.com/jeremieglbd/simple-terminal/-/archive/0.8.2/simple-terminal-0.8.2.tar.gz

BuildRequires: libX11, libXft, fontconfig, freetype-devel
#Requires:

%description

%global debug_package %{nil}

%prep
%setup


%build
make


%install
make DESTDIR=%{buildroot}/ install

%check


%files
%license
%doc
/usr/local/bin/st
/usr/local/bin/st-copyout
/usr/local/bin/st-urlhandler
/usr/local/share/man/man1/st.1

%changelog

